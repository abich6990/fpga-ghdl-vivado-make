set design [lindex $argv 0]
set num_threads [lindex $argv 1]

open_project /tmp/$design/$design.xpr

if {[catch {reset_run impl_1} result]} {
	puts "No need for resetting run impl_1"
} else {
	puts "Resetting run impl_1"
}

#################
# Place and route
#################

launch_runs -jobs $num_threads impl_1
wait_on_run impl_1
