set design [lindex $argv 0]
set num_threads [lindex $argv 1]

open_project /tmp/$design/$design.xpr

if {[catch {reset_run impl_1 -from_step write_bitstream} result]} {
	puts "No need for resetting run write_bitstream"
} else {
	puts "Resetting run write_bitstream"
}

#################
# Write Bitstream
#################

launch_runs -jobs $num_threads impl_1 -to_step write_bitstream
wait_on_run impl_1

write_cfgmem  -format bin -size 8 -interface SMAPx8 -loadbit {up 0x00000000 "/tmp/$design/$design.runs/impl_1/$design.bit" } -file "/tmp/$design/$design.bin"
