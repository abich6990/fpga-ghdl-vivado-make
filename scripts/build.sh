#!/bin/bash

SCRIPT_DIR=`dirname $0`
TOP_DEF=`basename $PWD`
TOP=${1:-$TOP_DEF}

PART="xc7z020clg400-1"
JOBS=`nproc`

VIVADO_PATH=/opt/xilinx/Vivado
VIVADO_VERSION=2017.4

echo "Vivado path set to ${VIVADO_PATH}"
echo "Vivado version set to ${VIVADO_VERSION}"
echo "Top module is $TOP"

source ${VIVADO_PATH}/${VIVADO_VERSION}/settings64.sh

if [ "$2" == "synth" ]; then
	vivado -tempDir /tmp/${TOP} -log /tmp/${TOP}/vivado.log -jou /tmp/${TOP}/vivado.jou -mode batch -source ${SCRIPT_DIR}/synth.tcl -tclargs ${TOP} ${JOBS} ${PART}

	REPORT="/tmp/${TOP}/${TOP}.runs/synth_1/${TOP}_utilization_synth.rpt"
	if [ -f "$REPORT" ]; then
		echo "|        Site Type        | Used | Fixed | Available | Util% |" | tr -s ' ' 
		cat ${REPORT} | grep "^|" | tr -s ' ' | awk 'BEGIN{FS="|"} {if ($6 > 0.0) print $0}'
	fi
elif [ "$2" == "impl" ]; then
	vivado -tempDir /tmp/${TOP} -log /tmp/${TOP}/vivado.log -jou /tmp/${TOP}/vivado.jou -mode batch -source ${SCRIPT_DIR}/impl.tcl -tclargs ${TOP} ${JOBS}

	REPORT="/tmp/${TOP}/${TOP}.runs/impl_1/${TOP}_utilization_placed.rpt"
	if [ -f "$REPORT" ]; then
		echo "|        Site Type        | Used | Fixed | Available | Util% |" | tr -s ' ' 
		cat ${REPORT} | grep "^|" | tr -s ' ' | awk 'BEGIN{FS="|"} {if ($6 > 0.0) print $0}'
	fi
elif [ "$2" == "bitstream" ]; then
	vivado -tempDir /tmp/${TOP} -log /tmp/${TOP}/vivado.log -jou /tmp/${TOP}/vivado.jou -mode batch -source ${SCRIPT_DIR}/bitstream.tcl -tclargs ${TOP} ${JOBS}
else
	vivado -tempDir /tmp/${TOP} -log /tmp/${TOP}/vivado.log -jou /tmp/${TOP}/vivado.jou -mode batch -source ${SCRIPT_DIR}/show.tcl -tclargs ${TOP}
fi
