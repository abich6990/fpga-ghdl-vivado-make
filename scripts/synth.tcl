set design [lindex $argv 0]
set num_threads [lindex $argv 1]
set partname [lindex $argv 2]

set root "."
set hdl_files [list $root/source/]
set xdc_file [list $root/constrain/]

if {[catch {create_project $design /tmp/$design -part $partname} result]} {
	open_project /tmp/$design/$design.xpr
	reset_run synth_1
	puts "Opening project /tmp/$design"
} else {
	puts "Creating project /tmp/$design"
}

#HDL
if {[string equal [get_filesets -quiet sources_1] ""]} {
    create_fileset -srcset sources_1
}
set_property default_lib xil_defaultlib [current_project]
set_property target_language VHDL [current_project]
read_vhdl -vhdl2008 -library xil_defaultlib [glob $hdl_files/*.vhdl]
read_xdc [glob $xdc_file/*.xdc]

set_property top $design [get_filesets sources_1]

###########
# Synthesis
###########

launch_runs -jobs $num_threads synth_1
wait_on_run synth_1
