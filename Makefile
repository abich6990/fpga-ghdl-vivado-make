SHELL:=/bin/bash

ifndef TOP
	TOP=hello_world_tb
endif

WORKDIR=/tmp/`basename ${PWD}`
SRCDIR=source
SCRIPTDIR=$(PWD)/scripts

GHDL=ghdl
GHDLFLAGS=--std=08 --workdir=$(WORKDIR) --ieee=synopsys

MODULES=$(wildcard $(SRCDIR)/entity/*.vhdl)
MODULES+=$(wildcard $(SRCDIR)/package/*.vhdl)
MODULES+=$(wildcard $(SRCDIR)/testbench/*.vhdl)
TESTS=$(wildcard $(SRCDIR)/testbench/*.vhdl)
TESTS_NP:=$(TESTS:.vhdl=)
TESTS_NP:=$(TESTS_NP:$(SRCDIR)/testbench/%=%)


simulation: analyse elaborate test
	@if [ "${SIM}" == "1" ] && ! [ pgrep -x "gtkwave" > /dev/null ]; then \
		bash -c "gtkwave $(WORKDIR)/$(TOP)_tb.ghw -a $(WORKDIR)/wave.sav &"; \
	fi
#	@gconftool-2 --type string --set /com.geda.gtkwave/0/reload 0

analyse:
	@mkdir -p $(WORKDIR)
	@$(GHDL) -i $(GHDLFLAGS) $(MODULES)

elaborate:
	@$(foreach TESTFILE,$(TESTS_NP),$(GHDL) -m $(GHDLFLAGS) $(TESTFILE);)

test:
	@$(foreach TESTFILE,$(TESTS_NP),$(GHDL) -r $(GHDLFLAGS) $(TESTFILE) --wave=$(WORKDIR)/$(TESTFILE).ghw;)


binary: synthesize implement bitstream

bitstream:
	@bash $(SCRIPTDIR)/build.sh ${TOP} bitstream

# check if TOP is a valid module i.e. file in SRCDIR
ifeq ($(findstring $(TOP),$(TESTS_NP)),)
	$(error TOP: '$(TOP)' is not a valid module for synthetisation/implementation)
endif

synthesize:
	@bash $(SCRIPTDIR)/build.sh ${TOP} synth

# check if TOP is a valid module i.e. file in SRCDIR
ifeq ($(findstring $(TOP),$(TESTS_NP)),)
	$(error TOP: '$(TOP)' is not a valid module for synthetisation/implementation)
endif

implement:
	@bash $(SCRIPTDIR)/build.sh ${TOP} impl

# check if TOP is a valid module i.e. file in SRCDIR
ifeq ($(findstring $(TOP),$(TESTS_NP)),)
	$(error TOP: '$(TOP)' is not a valid module for synthetisation/implementation)
endif

show:
	@bash $(SCRIPTDIR)/build.sh ${TOP}

# check if TOP is a valid module i.e. file in SRCDIR
ifeq ($(findstring $(TOP),$(TESTS_NP)),)
	$(error TOP: '$(TOP)' is not a valid module for synthetisation/implementation)
endif

clean:
	@rm -rf $(WORKDIR)
	@rm -rf /tmp/$(TOP)
	@rm *_tb* 2>/dev/null || exit 0



