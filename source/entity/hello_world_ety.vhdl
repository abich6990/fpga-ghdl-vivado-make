-- copied and modified from https://github.com/leachim6/hello-world/blob/master/v/vhdl.vhdl
use std.textio.all;


entity hello_world_ety is
end entity;

architecture behavior of hello_world_ety is
begin
	process
	begin
		write(output, String'("Hello world!"));
		wait;
	end process;
end architecture;

