library ieee;
use ieee.std_logic_1164.all;


entity hello_world_tb is
end entity;

architecture behavior of hello_world_tb is
	signal s_hello : boolean := FALSE;
begin
	process
	begin
		wait for 5 ns;
		s_hello <= TRUE;
		wait for 5 ns;
		assert s_hello = TRUE report "s_hello should be true!";
		
		wait for 5 ns;
		s_hello <= FALSE;
		wait for 5 ns;
		assert s_hello = FALSE report "s_hello should be false!";	
		wait;
	end process;	
end architecture;

